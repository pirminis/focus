# Focus on work

## What it does

Add entries to you hosts file so you cannot access websites that drain you attention, like facebook, twitter, youtube, etc.

## Preparation

Create duplicate of `/private/etc/hosts` file:
```
(cd /private/etc && cp hosts hosts.focus)
```

Modify `/private/etc/hosts.focus` file by adding entries of website you want to block:
```
echo "127.0.0.1 facebook.com" | sudo tee -a /private/etc/hosts.focus
```

Create shell aliases:
```
alias focus="/path/to/this/repository/focus"
alias relax="/path/to/this/repository/relax"
```

Reload your shell script or terminal and you are ready to go!

## Using

To focus on your work just type:
```
focus
```

and enter your password.

To relax from work just type:
```
relax
```

and enter your password.
